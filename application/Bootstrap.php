<?php

class Bootstrap extends La_Application_Bootstrap
{
    public function _initRegistryDb()
    {
        $this->bootstrap('multidb');
        $db = $this->getPluginResource('multidb')->getDb('pgsql');
        Zend_Registry::set('db-moodle', $db);
    }
    
}