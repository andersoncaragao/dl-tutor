<?php

class Auth_IndexController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $this->_helper->layout()->setLayout('public');
        $this->view->form = new Auth_Form_Login();
    }

    public function loginAction()
    {
        if ($this->getRequest()->isPost()) {
            $form = new Auth_Form_Login();
            $data = $this->getRequest()->getParams();

            if ($form->isValid($data)) {
                $user     = $form->getValue('email');
                $password = $form->getValue('pass');
                $adapter  = $this->getAdapter($user, $password);
                $auth     = Zend_Auth::getInstance();
                $result   = $auth->authenticate($adapter);
                
                if ($result->isValid()) {
                    $omit = array('password', 'salt');
                    $info = $adapter->getResultRowObject(null, $omit);
                    
                    $auth->getStorage()->write($info);

                    if ($info->role == 'User') {
                        $userEnterprise = new \Auth_Model_DbTable_UserEnterprise();
                        $empresaId      = $userEnterprise->enterpriseByUser($info->id);
                        
                        if (!$empresaId) {
                            $this->redirect('/auth/index/logout');
                        }
                        
                        $info->empresa_id   = $empresaId;
                        $auth->getStorage()->write($info);
                        $this->redirect('/');
                    }

                    $this->redirect('/');
                }
            }
        }

        $this->_helper->message('ERROR_LOGIN');
        $this->_redirect('/auth');
    }

    public function logoutAction()
    {
        $session = new Zend_Session_Namespace('login');
        $session->unsetAll();
        
        Zend_Auth::getInstance()->clearIdentity();
        
        $this->_redirect('/auth');
    }
    
    protected function getAdapter($user, $password)
    {
        $adapter = new Zend_Auth_Adapter_DbTable(null, 'auth_user', 'email', 'password');
        $adapter->setIdentity($user)
                ->setCredential($password)
                ->setCredentialTreatment("SHA1(SHA1(SHA1(CONCAT(?, salt))))");

        $adapter->getDbSelect()->join(array('r' => 'auth_role'), 'auth_user.auth_role_id = r.id', array('role' => 'name'));

        return $adapter;
    }
}