<?php

class Auth_ResourceRoleController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->title = "Permissões de acesso";
    }
    
    public function indexAction()
    {
        $headers = array('name');
        $table   = new Auth_Model_DbTable_Role();
     
        $this->_helper->list($headers, null, $table);
    }
    
    public function formAction()
    {
        $id       = \Zend_Filter::filterStatic($this->getParam("id"), 'int');
        $table    = new Auth_Model_DbTable_RoleResource();
        $resource = new Auth_Model_DbTable_Resource();
        
        $this->view->id          = $id;
        $this->view->permissions = $table->fetchByPerfil($id);
        $this->view->resources   = $resource->fetchAll(null, array('module', 'privilege'));
    }
    
    public function saveAction()
    {
        $usuarioGrupoId = \Zend_Filter::filterStatic($this->getParam("auth_role_id"), 'int');
        $resources      = $this->getParam("auth_resource_id");

        $table = new Auth_Model_DbTable_RoleResource();
        $where = array('auth_role_id = ?' => $usuarioGrupoId);
        
        $table->delete($where);
        
        foreach ($resources as $resource) {
            $resourceId = \Zend_Filter::filterStatic($resource, 'int');
            $table->insert(array('auth_role_id' => $usuarioGrupoId,
                                 'auth_resource_id' => $resourceId));
        }
        
        if (\Zend_Registry::isRegistered('cache')) {
            $cache = \Zend_Registry::get('cache');
            $cache->remove('acl');
        }
        
        $this->_redirect('auth/resource-role/form/id/' . $usuarioGrupoId);
    }
}