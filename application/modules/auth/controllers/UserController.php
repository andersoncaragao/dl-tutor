<?php

class Auth_UserController extends Zend_Controller_Action
{
    public function init()
    {
        $this->table = new Auth_Model_DbTable_User();
        $this->form  = new Auth_Form_User(array('table' => $this->table));
    }
    
    public function saveAction()
    {
        $this->setPasswordNotRequired();
        
        if (trim($this->getParam('password'))) {
            $salt = $this->generateSalt(60);
            $password = sha1(sha1(sha1($this->getParam('password').$salt)));
            $cpassword = sha1(sha1(sha1($this->getParam('password_confirmacao').$salt)));
            
            $this->_request->setParam('salt', $salt);
            $this->_request->setParam('password', $password);
            $this->_request->setParam('password_confirmacao', $cpassword);
        } else {
            $this->form->removeElement('salt');
            $this->form->removeElement('password');
            $this->form->removeElement('password_confirmacao');
        }
        
        $this->_helper->save();
    }
    
    public function indexAction()
    {
        $headers    = array('id' => 'Código','name' => 'Nome', 'email' => 'E-mail');
        $elements   = array('name', 'email');
        $this->form = new Auth_Form_User(array('table'  => $this->table, 'fields' => $elements));
        
        $this->form->removeElement('auth_role_id');
        $this->_helper->list($headers);
    }
    
    public function formAction()
    {
//        $this->form->removeElement('auth_role_id');
        $this->_helper->form();
    }
    
    public function generateSalt($max = 15) 
    {
        $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
        $i             = 0;
        $salt          = "";
        
        while ($i < $max) {
            $salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
            $i++;
        }
        return $salt;
    }
    
    public function setPasswordNotRequired()
    {
        if ((int) $this->getParam('id')) {
            $this->form->salt->setRequired(false);
            $this->form->password->setRequired(false);
            $this->form->password_confirmacao->setRequired(false);
        
            $this->form->salt->removeValidator('NotEmpty')
                             ->removeValidator('StringLength');
        
            $this->form->password->removeValidator('NotEmpty')
                                 ->removeValidator('StringLength');
        
            $this->form->password_confirmacao->removeValidator('NotEmpty')
                                             ->removeValidator('StringLength');
        }
    }
}