<?php

class Auth_Form_Login extends La_Form
{
    public function init()
    {
        $this->setAction('auth/index/login');
        
        $this->addElement('text', 'email')
             ->addElement('password', 'pass')
             ->addElement('submit', 'Enviar');
    }
}