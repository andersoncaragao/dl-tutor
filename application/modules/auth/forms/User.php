<?php

class Auth_Form_User extends La_Form
{
    public function init()
    {
        $this->setAction('auth/user/save');
    }
    
    public function elementsForFilter()
    {
        $this->setAction('auth/user/index/table/auth_user');
        parent::elementsForFilter();
    }
    
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->removeElement('salt');
        $this->password->addDecorator('HtmlTag', array('class' => 'clear'));
        $this->setPasswordNotRequired();
        
        return parent::render($view);
    }
    
    public function setPasswordNotRequired()
    {
        if ($this->id->getValue()) {
            $this->password->setRequired(false);
            $this->password_confirmacao->setRequired(false);
            
            $this->password->removeValidator('NotEmpty')
                           ->removeValidator('StringLength');
            
            $this->password_confirmacao->removeValidator('NotEmpty')
                                       ->removeValidator('StringLength');
            
            $this->removeValidator('password','NotEmpty');
        }
    }
}