<?php

class Auth_Model_DbTable_Resource extends La_Db_Table
{
    protected $_name = "auth_resource";

    public function getDistinctModules()
    {
        $select = $this->getAdapter()->select()->distinct()->from($this->_name, 'module');

        return $this->getAdapter()->fetchAll($select);
    }
}