<?php

class Auth_Model_DbTable_RoleResource extends La_Db_Table
{
    protected $_name = "auth_role_resource";
    protected $_primary = array('auth_role_id','auth_resource_id');

    public function fetchAllRelations()
    {
        $select = $this->joinWith(array('r' => 'auth_resource', 'up' => 'auth_role'),
                                  array('r.*','up.*'));
        return $this->fetchAll($select);
    }

    public function fetchByPerfil($perfilId)
    {
        $select = $this->joinWith(array('r' => 'auth_resource', 'up' => 'auth_role'),
                                  array('r.*','up.*'));

        $select->where('auth_role_id = ?', $perfilId);
        
        return $this->fetchAll($select);
    }
}