<?php

class Auth_Model_DbTable_UserEnterprise extends La_Db_Table
{
    protected $_name = "usuario_empresa";
    
    public function userHasEnterprise($userId, $empresaId = null)
    {
        $select = $this->select()->from($this->_name, 'empresa_id')
                    ->where('usuario_id = ?', $userId)
                    ->where('excluido = 0');
        
        if ($empresaId) {
            $select->where('empresa_id = ?', $empresaId);
        }
        
        $data = $this->getAdapter()->fetchAll($select);
        
        if ($data) {
            return true;
        }
        return false;
    }
    
    public function enterpriseByUser($userId)
    {
        $select = $this->select()->from($this->_name, 'empresa_id')
                    ->where('usuario_id = ?', $userId)
                    ->where('excluido = 0');
        
        $data = $this->getAdapter()->fetchRow($select);
        
        if ($data) {
            return $data['empresa_id'];
        }
        
        return false;
    }
}