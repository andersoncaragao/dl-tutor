<?php
class Auth_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if (Zend_Registry::isRegistered('cache')) {
            $cache = Zend_Registry::get('cache');
            $acl   = $cache->load('acl');
        }
        
        if (!$acl) {
            $acl          = new Zend_Acl();
            $role         = new Auth_Model_DbTable_Role();
            $resource     = new Auth_Model_DbTable_Resource();
            $roleResource = new Auth_Model_DbTable_RoleResource();
            
            $roles     = $role->fetchAll("name <> 'Todos'");
            $resources = $resource->getDistinctModules(); 
            $relations = $roleResource->fetchAllRelations();
            
            $acl->addRole('Todos');
            
            foreach ($roles as $role) {
                $acl->addRole($role['name'], 'Todos');
            }
            
            foreach ($resources as $resource) {
                $acl->addResource($resource['module']);              
            }
            
            foreach ($relations as $relation) {
                $acl->allow($relation['name'], $relation['module'], $relation['privilege']);
            }
            
            if (Zend_Registry::isRegistered('cache')) {
                $cache->save($acl, 'acl');
            }
        }
        
        Zend_Registry::set('acl', $acl);
    }
}
    