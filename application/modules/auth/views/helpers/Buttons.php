<?php

class Zend_View_Helper_Buttons extends Zend_View_Helper_Abstract
{
    public function buttons($name, $value, $options)
    {
        return sprintf('<div class="row-fluid">
                        <button type="reset" class="span6 btn btn-small">
                            <i class="icon-refresh"></i>
                            Limpar
                        </button>
                        <button type="submit" name="submit" id="submit" class="span6 btn btn-small btn-success">
                            %s
                            <i class="icon-arrow-right icon-on-right"></i>
                        </button>
                    </div>', $options['text']);
    }
}