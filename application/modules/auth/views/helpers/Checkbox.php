<?php

class Zend_View_Helper_Checkbox extends Zend_View_Helper_Abstract
{
    public function checkbox($name, $value, $options)
    {
        return sprintf('<label>
                            <input type="checkbox" value="1" name="%1$s" id="%1$s">
                            <span class="lbl"> %2$s</span>
                        </label>', $name, $options['text']);
    }
}