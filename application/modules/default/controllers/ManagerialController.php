<?php

class Default_ManagerialController extends Zend_Controller_Action
{
    public function init()
    {
        $this->courseCategories = new Default_Model_DbTable_CourseCategories();
        
    }

    public function tutorTeamAction()
    {
        $form     = new Default_Form_TutorTeam();
        $dbCourse = new Default_Model_DbTable_Course();
        $this->view->title = 'Distribuição de Tutores por Turma';
        
        $data        = [];
        $disciplines = [];

        if ($this->getRequest()->isPost()) {
            $course  = $this->getParam('course');
            $period  = $this->getParam('period');
            $visible = $this->getParam('visible');
            
            if ($course && $period) {
                
                $options = $this->courseCategories->fetchOptions(['course_id' => $course], 'period');
                $form->period->addMultiOptions($options);
                
                $select = $this->courseCategories->getDisciplines()
                        ->where('course_id = ?', $course)
                        ->where('period_id = ?', $period)
                        ->where('discipline.visible = ?', $visible)
                        ;
                $disciplines = $this->courseCategories->getAdapter()->fetchAll($select);

                if ($disciplines) {
                    $ids = [];
                    foreach ($disciplines as $value) {
                        $ids[] = $value['discipline_id'];
                    }
                }

                $data = $dbCourse->getAdapter()->fetchAll($dbCourse->tutorXTurma()->where('c.id IN (?)', $ids));
                
                $form->populate($this->getAllParams());
            }

        }
        
        $this->view->titleGraph = 'Tutores por Disciplina';
        
        $this->view->headers = [ 'discipline'     => 'Disciplinas', 
                                 'students'       => 'Quant. Alunos',    
                                 'tutors_ead'     => 'Quant. Tutores EAD',   
                                 'tutorsXstudent' => 'Média Alunos x TD'
            ];
        $this->view->form      = $form;
        $this->view->dataGraph = json_encode($data);
        $this->view->data      = $data;
        
    }
    
    public function listPeriodsAction()
    {
        $this->_helper->layout()->disableLayout();
        $course = $this->getParam('course');
        
        $data[] = '[selecione]';
        $this->view->data = $data;
        
        if ($course) {            
            $periods = $this->courseCategories->fetchOptions(['course_id' => $course], 'period');           
            
            $this->view->data = array_replace($data, $periods);
        }
        
    }
    
    public function listSemestersAction()
    {
        $this->_helper->layout()->disableLayout();
        $course = $this->getParam('course');
        
        $data[] = '[selecione]';
        $this->view->data = $data;
        
        if ($course) {            
            $periods = $this->courseCategories->fetchOptions(['course_id' => $course], 'semester');           
            
            $this->view->data = array_replace($data, $periods);
        }
        
    }
    
    public function listDisciplinesAction()
    {
        $this->_helper->layout()->disableLayout();
        $semester = $this->getParam('period');
        $course   = $this->getParam('course');

        $data[] = '[selecione]';
        $this->view->data = $data;
        
        if ($semester) {            
            $periods = $this->courseCategories->fetchOptions(["substring(fullname from '......$')" => $semester, 'course_id' => $course], 'discipline');           
            $this->view->data = array_replace($data, $periods);
        }
        
    }

    public function tutorPoloAction()
    {
        $form     = new Default_Form_TutorPolo();
        $dbCourse = new Default_Model_DbTable_Course();
        
        $this->view->title = 'Tutores Presenciais por Polo do Curso';

        $data        = [];
        $disciplines = [];

        if ($this->getRequest()->isPost()) {
            $course     = $this->getParam('course');
            $period     = $this->getParam('period');
            $discipline = $this->getParam('discipline');
            
            if ($course && $period) {
                
                $options = $this->courseCategories->fetchOptions(['course_id' => $course], 'semester');
                $form->period->addMultiOptions($options);
                
                $select = $this->courseCategories->getDisciplines()
                        ->where('course_id = ?', $course)
                        ->where("substring(fullname from '......$') = ?", $period)
                        ;

                $disciplines = $this->courseCategories->getAdapter()->fetchAll($select);
                
                if ($disciplines) {
                    $ids = [];
                    foreach ($disciplines as $value) {
                        $ids[] = $value['discipline_id'];
                    }
                }
                
                $data = $dbCourse->getAdapter()->fetchAll($dbCourse->tutorXPolo($ids));

                $form->populate($this->getAllParams());
            }

        }
        
        $this->view->titleGraph = 'Tutores por Polo';
        
       $this->view->headers = [ 'polo'          => 'Polo', 
                                 'tutors'        => 'Quant. Tutores',   
                                 'students'      => 'Quant. Alunos',    
                                 'tutorXstudent' => 'Média Alunos x TP'
            ];
       
        $this->view->form      = $form;
        $this->view->dataGraph = json_encode($data);
        $this->view->data      = $data;
        
    }
    
    public function tutorAccessAction()
    {
        $form     = new \Default_Form_TutorAccess();
        $dbCourse = new \Default_Model_DbTable_CourseCategories();
        $params   = $this->getRequest()->getParams();
        $dbTutor  = new \Default_Model_DbTable_Tutor();
        
        $tutorHits['data']        = [];
        $tutorHits['labels']      = [];
        $tutorHits['labelsGraph'] = [];
        
        if ($this->getRequest()->IsPost()) {
            
            if ($params['data_de']) {
                $timeInitial = (new Zend_Date($params['data_de'], 'pt_BR'))->get(Zend_Date::TIMESTAMP);
            }
            
            if ($params['data_a']) {
                $timeFinal = (new Zend_Date($params['data_a'], 'pt_BR'))->get(Zend_Date::TIMESTAMP);
            }
            
            if ($params['discipline']) {
                $discipline = $params['discipline'];
            }
            
            $tutorHits = $dbTutor->tutorHit($discipline, $timeInitial, $timeFinal);
            
            $options   = $this->courseCategories->fetchOptions(['course_id' => $params['course']], 'semester');
            $form->period->addMultiOptions($options);
            
            $discipline = $dbCourse->getAdapter()->fetchRow($dbCourse->getDisciplines()->where('id = ?', $params['discipline']));
            $this->view->discipline = $discipline['discipline_name'];
            
            $disciplines = $this->courseCategories->fetchOptions(["substring(fullname from '......$')" => $params['period'], 'course_id' => $params['course']], 'discipline');
            $form->discipline->addMultiOptions($disciplines);
            
            $this->view->title = "Regularidade de acesso dos tutores à distância ({$params['data_de']} à {$params['data_a']})";
            $this->view->titleGraph = "{$discipline['discipline_name']}";
        }
        
        $data                    = $tutorHits['data'];
        $this->view->headers     = $tutorHits['labels'];
        $this->view->labelGraph  = json_encode($tutorHits['labelsGraph']);
        $this->view->dataGraph   = json_encode($data);
        
        $this->view->data = $data;
        $this->view->form = $form->populate($params);
        
    }
    
    function m2h($mins) {
        if ($mins < 0) {
            $min = abs($mins);
        } else {
            $min = $mins;
        }

        $h = floor($min / 60);
        $m = ($min - ($h * 60)) / 100;
        $horas = $h + $m;
 
        if ($mins < 0) {
            $horas *= -1;
        }

        $sep = explode('.', $horas);
        $h = $sep[0];
        if (empty($sep[1]))
            $sep[1] = 00;
 
        $m = $sep[1];
 
        if (strlen($m) < 2) {
            $m = $m . 0;
        }

        return sprintf('%02d:%02d', $h, $m);
    }
}
