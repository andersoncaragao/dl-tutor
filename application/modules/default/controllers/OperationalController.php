<?php

class Default_OperationalController extends Zend_Controller_Action
{
    public function init()
    {
    }

    public function tutorHitsAction()
    {
        
        $this->view->title = 'Acesso dos Tutores x total de hits (01/03/2014 à 07/03/2014)';
        $this->view->titleGraph = 'Licenciatura em Computação (CPT) 2014.1 ';
//        $form     = new Default_Form_TutorPolo();
        
        $data = [
            ['tutor'=> 'Tutor 1', 'curso'=> 1, 'chat'=> 64, 'blog' => 13, 'forum' => 5, 'perfil' => 30, 'recursos' => 25, 'tarefas' =>0, 'wiki' => 0], 
            ['tutor'=> 'Tutor 2', 'curso'=> 2, 'chat'=> 52, 'blog' => 12, 'forum' => 15, 'perfil' => 4, 'recursos' => 31, 'tarefas' => 1, 'wiki' => 0], 
            ['tutor'=> 'Tutor 3', 'curso'=> 3, 'chat'=> 33, 'blog' => 2, 'forum' => 1, 'perfil' => 34, 'recursos' => 5, 'tarefas' => 10, 'wiki' => 1], 
        ];
        
        $this->view->headers = [ 'tutor'   => 'Tutor', 
                                 'curso'   => 'Curso',
                                 'chat'    => 'Chat',    
                                 'blog'    => 'Blog',   
                                 'forum'   => 'Fórum',
                                 'perfil'  => 'Perfil',
                                 'recursos' => 'Recurso',
                                 'tarefas' => 'Tarefas',
                                 'wiki'    => 'Wiki'
            ];
        
        $this->view->dataGraph = json_encode($data);
        $total = ['tutor'=> 'Total', 'curso'=> 0, 'chat'=> 0, 'blog' => 0, 'forum' => 0, 'perfil' => 0, 'recursos' => 0, 'tarefas' => 0, 'wiki' => 0];
        
        foreach ($data as $key => $value) {
            $total['curso']     += $value['curso'];
            $total['chat']      += $value['chat'];
            $total['blog']      += $value['blog'];
            $total['forum']     += $value['forum'];
            $total['perfil']    += $value['perfil'];
            $total['recursos']  += $value['recursos'];
            $total['tarefas']   += $value['tarefas'];
            $total['wiki']      += $value['wiki'];
        }
        
//        $this->view->form      = $form;
        $data[] = $total;
        $this->view->data = $data;
        
    }
    
    public function studentsServedAction()
    {
        
        $this->view->title = 'Quantidade de Alunos atendidos pelos Tutores à Distância (01/03/2014 à 07/03/2014)';
        $this->view->titleGraph = 'Licenciatura em Computação (CPT) Introdução a Computação 2014.1 ';
//        $form     = new Default_Form_TutorPolo();
        
 ;       $data = [
            ['dia'=> '1/3/2014', 'tutor1'=> 3, 'tutor2'=> 2, 'tutor3' => 1], 
            ['dia'=> '2/3/2014', 'tutor1'=> 14, 'tutor2'=> 2, 'tutor3' => 16], 
            ['dia'=> '3/3/2014', 'tutor1'=> 12, 'tutor2'=> 16, 'tutor3' => 23], 
            ['dia'=> '4/3/2014', 'tutor1'=> 2, 'tutor2'=> 1, 'tutor3' => 1], 
            ['dia'=> '5/3/2014', 'tutor1'=> 2, 'tutor2'=> 2, 'tutor3' => 3], 
            ['dia'=> '6/3/2014', 'tutor1'=> 24, 'tutor2'=> 21, 'tutor3' => 19], 
            ['dia'=> '7/3/2014', 'tutor1'=> 74, 'tutor2'=> 56, 'tutor3' => 77], 
        ];
        
        $this->view->headers = [ 'dia'    => 'Data', 
                                 'tutor1' => 'Tutor - 1',    
                                 'tutor2' => 'Tutor - 2',   
                                 'tutor3' => 'Tutor - 3'
            ];
        
        $this->view->dataGraph = json_encode($data);
        $total = ['dia' => 'Total', 'tutor1' => 0, 'tutor2' => 0, 'tutor3' => 0];  
        
        foreach ($data as $key => $value) {
            $total['tutor1'] += $value['tutor1'];
            $total['tutor2'] += $value['tutor2'];
            $total['tutor3'] += $value['tutor3'];
        }
        
//        $this->view->form      = $form;
        $data[] = $total;
        $this->view->data = $data;
    }
    
    public function monitoringTaskAction()
    {
        $this->view->title = 'Acompanhamento dos Tarefas pelos Tutores à Distância (01/03/2014 à 07/03/2014)';
        $this->view->titleGraph = 'Licenciatura em Computação (CPT) Introdução a Computação 2014.1 ';
//        $form     = new Default_Form_TutorPolo();
        
       $data = [
            ['polo'=> 'CAB', 'tutor'=> 'CAB - tutor1', 'alunoXpolo'=> 36, 'participantes' => 25, 'corrigidas' => 20, 'naoCorrigidas' => 5], 
            ['polo'=> 'CON', 'tutor'=> 'CON - tutor1', 'alunoXpolo'=> 46, 'participantes' => 34, 'corrigidas' => 31, 'naoCorrigidas' => 3], 
            ['polo'=> 'SBE', 'tutor'=> 'SBE - tutor1', 'alunoXpolo'=> 20, 'participantes' => 20, 'corrigidas' => 20, 'naoCorrigidas' => 0], 
            ['polo'=> 'CDM', 'tutor'=> 'CDM - tutor2', 'alunoXpolo'=> 22, 'participantes' => 17, 'corrigidas' => 13, 'naoCorrigidas' => 4], 
            ['polo'=> 'COR', 'tutor'=> 'COR - tutor2', 'alunoXpolo'=> 19, 'participantes' => 19, 'corrigidas' => 10, 'naoCorrigidas' => 9], 
            ['polo'=> 'PAR', 'tutor'=> 'PAR - tutor2', 'alunoXpolo'=> 44, 'participantes' => 27, 'corrigidas' => 15, 'naoCorrigidas' => 12], 
            ['polo'=> 'CGR', 'tutor'=> 'CGR - tutor3', 'alunoXpolo'=> 48, 'participantes' => 26, 'corrigidas' => 22, 'naoCorrigidas' => 4], 
            ['polo'=> 'ITG', 'tutor'=> 'ITG - tutor3', 'alunoXpolo'=> 40, 'participantes' => 35, 'corrigidas' => 33, 'naoCorrigidas' => 2], 
            ['polo'=> 'TAP', 'tutor'=> 'TAP - tutor3', 'alunoXpolo'=> 50, 'participantes' => 43, 'corrigidas' => 41, 'naoCorrigidas' => 2], 
        ];
        
        $this->view->headers = [ 'tutor' => 'Polo - Tutor',    
                                 'alunoXpolo' => 'Qt. Alunos',   
                                 'participantes' => 'Participantes',
                                 'corrigidas' => 'Corrigidas',
                                 'naoCorrigidas' => 'Não corrgidas',
            ];
        
        $this->view->dataGraph = json_encode($data);
        
        
//        $this->view->form      = $form;
        $this->view->data = $data;
    }
    
    public function monitoringForumAction()
    {
        $this->view->title = 'Acompanhamento dos Fóruns pelos Tutores à Distância (01/03/2014 à 07/03/2014)';
        $this->view->titleGraph = 'Licenciatura em Computação (CPT) Introdução a Computação 2014.1 ';
//        $form     = new Default_Form_TutorPolo();
        
       $data = [
            ['polo'=> 'CAB', 'tutor'=> 'CAB - tutor1', 'alunoXpolo'=> 36, 'participantes' => 25, 'corrigidas' => 20, 'naoCorrigidas' => 5], 
            ['polo'=> 'CON', 'tutor'=> 'CON - tutor1', 'alunoXpolo'=> 46, 'participantes' => 34, 'corrigidas' => 31, 'naoCorrigidas' => 3], 
            ['polo'=> 'SBE', 'tutor'=> 'SBE - tutor1', 'alunoXpolo'=> 20, 'participantes' => 20, 'corrigidas' => 20, 'naoCorrigidas' => 0], 
            ['polo'=> 'CDM', 'tutor'=> 'CDM - tutor2', 'alunoXpolo'=> 22, 'participantes' => 17, 'corrigidas' => 13, 'naoCorrigidas' => 4], 
            ['polo'=> 'COR', 'tutor'=> 'COR - tutor2', 'alunoXpolo'=> 19, 'participantes' => 19, 'corrigidas' => 10, 'naoCorrigidas' => 9], 
            ['polo'=> 'PAR', 'tutor'=> 'PAR - tutor2', 'alunoXpolo'=> 44, 'participantes' => 27, 'corrigidas' => 15, 'naoCorrigidas' => 12], 
            ['polo'=> 'CGR', 'tutor'=> 'CGR - tutor3', 'alunoXpolo'=> 48, 'participantes' => 26, 'corrigidas' => 22, 'naoCorrigidas' => 4], 
            ['polo'=> 'ITG', 'tutor'=> 'ITG - tutor3', 'alunoXpolo'=> 40, 'participantes' => 35, 'corrigidas' => 33, 'naoCorrigidas' => 2], 
            ['polo'=> 'TAP', 'tutor'=> 'TAP - tutor3', 'alunoXpolo'=> 50, 'participantes' => 43, 'corrigidas' => 41, 'naoCorrigidas' => 2], 
        ];
        
        $this->view->headers = [ 'tutor' => 'Polo - Tutor',    
                                 'alunoXpolo' => 'Qt. Alunos',   
                                 'participantes' => 'Participantes',
                                 'corrigidas' => 'Corrigidas',
                                 'naoCorrigidas' => 'Não corrgidas',
            ];
        
        $this->view->dataGraph = json_encode($data);
        
        
//        $this->view->form      = $form;
        $this->view->data = $data;
        
    }
    
    public function monitoringSurveyAction()
    {
        $this->view->title = 'Acompanhamento dos Questionários pelos Tutores à Distância (01/03/2014 à 07/03/2014)';
        $this->view->titleGraph = 'Licenciatura em Computação (CPT) Introdução a Computação  2014.1 ';
//        $form     = new Default_Form_TutorPolo();
        
       $data = [
            ['polo'=> 'CAB', 'tutor'=> 'CAB - tutor1', 'alunoXpolo'=> 36, 'participantes' => 25, 'corrigidas' => 20, 'naoCorrigidas' => 5], 
            ['polo'=> 'CON', 'tutor'=> 'CON - tutor1', 'alunoXpolo'=> 46, 'participantes' => 34, 'corrigidas' => 31, 'naoCorrigidas' => 3], 
            ['polo'=> 'SBE', 'tutor'=> 'SBE - tutor1', 'alunoXpolo'=> 20, 'participantes' => 20, 'corrigidas' => 20, 'naoCorrigidas' => 0], 
            ['polo'=> 'CDM', 'tutor'=> 'CDM - tutor2', 'alunoXpolo'=> 22, 'participantes' => 17, 'corrigidas' => 13, 'naoCorrigidas' => 4], 
            ['polo'=> 'COR', 'tutor'=> 'COR - tutor2', 'alunoXpolo'=> 19, 'participantes' => 19, 'corrigidas' => 10, 'naoCorrigidas' => 9], 
            ['polo'=> 'PAR', 'tutor'=> 'PAR - tutor2', 'alunoXpolo'=> 44, 'participantes' => 27, 'corrigidas' => 15, 'naoCorrigidas' => 12], 
            ['polo'=> 'CGR', 'tutor'=> 'CGR - tutor3', 'alunoXpolo'=> 48, 'participantes' => 26, 'corrigidas' => 22, 'naoCorrigidas' => 4], 
            ['polo'=> 'ITG', 'tutor'=> 'ITG - tutor3', 'alunoXpolo'=> 40, 'participantes' => 35, 'corrigidas' => 33, 'naoCorrigidas' => 2], 
            ['polo'=> 'TAP', 'tutor'=> 'TAP - tutor3', 'alunoXpolo'=> 50, 'participantes' => 43, 'corrigidas' => 41, 'naoCorrigidas' => 2], 
        ];
        
        $this->view->headers = [ 'tutor' => 'Polo - Tutor',    
                                 'alunoXpolo' => 'Qt. Alunos',   
                                 'participantes' => 'Participantes',
                                 'corrigidas' => 'Corrigidas',
                                 'naoCorrigidas' => 'Não corrgidas',
            ];
        
        $this->view->dataGraph = json_encode($data);
        
        
//        $this->view->form      = $form;
        $this->view->data = $data;
        
    }
}
