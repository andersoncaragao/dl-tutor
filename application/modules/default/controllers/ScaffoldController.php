<?php

class Default_ScaffoldController extends Zend_Controller_Action
{
    public function init()
    {
        $table  = $this->_getParam('table');
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/modules/default/configs/columns.ini');
        
        $this->config = $config->toArray();
        $this->table  = new La_Db_Table($table);
        
        $formOptions = array('table' => $this->table);
        
        if (isset($this->config[$table]['form'])) {
            $formOptions['fields'] = array_map('trim', explode(",", $this->config[$table]['form']));
        }
        
        $this->table->setAutoJoin(true);
        
        $this->form = $this->getForm($formOptions);

        $this->view->title = isset($this->config[$table]['title']) ? $this->config[$table]['title'] : ucfirst($table);
        $this->view->table = $table;
    }

    public function indexAction()
    {
        $table         = $this->_getParam('table');
        $listColumns   = $this->table->getColumns();
        
        if (isset($this->config[$table])) {
            if (isset($this->config[$table]['list'])) {
                $listColumns = array_map('trim', explode(",", $this->config[$table]['list']));
            }
            
            if (isset($this->config[$table]['filter'])) {
                $fieldsColumns = array_map('trim', explode(",", $this->config[$table]['filter']));
                $this->form = $this->getForm(array('table' => $this->table, 'fields' => $fieldsColumns));
            }
        }
        
        $this->_helper->list($listColumns);
        
        $scriptPath = "%s/modules/default/views/scripts/%s/index.phtml";
        
        if (is_readable(sprintf($scriptPath, APPLICATION_PATH, $table))) {
            $this->renderScript(sprintf("%s/index.phtml", $table));
        }
    }
    
    public function listAction()
    {
        $table = $this->_getParam('table');
        $listColumns = $this->table->getColumns();
        
        if (isset($this->config[$table])) {
            if (isset($this->config[$table]['list'])) {
                $listColumns = array_map('trim', explode(",", $this->config[$table]['list']));
            }
            
            if (isset($this->config[$table]['filter'])) {
                $fieldsColumns = array_map('trim', explode(",", $this->config[$table]['filter']));
                $this->form = $this->getForm(array('table' => $this->table, 'fields' => $fieldsColumns));
            }
        }
        
        $this->_helper->list($listColumns);
        
        
        $scriptPath = "%s/modules/default/views/scripts/%s/list.phtml";
        
        if (is_readable(sprintf($scriptPath, APPLICATION_PATH, $table))) {
            $this->renderScript(sprintf("%s/list.phtml", $table));
        }
    }

    public function formAction()
    {
        $table = $this->_getParam('table');
        
        $this->_helper->form();
        
        $scriptPath   = "%s/modules/default/views/scripts/%s/form.phtml";
        $filter       = new Zend_Filter_Word_UnderscoreToDash();
        $scriptFolder = $filter->filter($table);
        
        if (is_readable(sprintf($scriptPath, APPLICATION_PATH, $scriptFolder))) {
            $this->renderScript(sprintf("%s/form.phtml", $scriptFolder));
        }
    }

    public function saveAction()
    {
        $this->_helper->save();
    }

    public function deleteAction()
    {
        $this->_helper->delete();
    }
    
    protected function getForm($options)
    {
        $table = $this->_getParam('table');
        
        $filter = new Zend_Filter_Word_UnderscoreToCamelCase();
        $className = "Default_Form_" . $filter->filter($table);

        if (class_exists($className)) {
            return new $className($options);
        }

        $form = new La_Form($options);
        $form->setAction('scaffold/save/table/' . $table);
        
        return $form;
    }
}