<?php

class Default_Form_TutorPolo extends La_Form
{
     public function init() 
    {
        $this->setMethod('POST');
        $this->setAction('default/managerial/tutor-polo');
        
        $courseDb = new Default_Model_DbTable_CourseCategories();
        $options = $courseDb->fetchOptions();
        
        $course = (new \Zend_Form_Element_Select('course'))
            ->setLabel('Curso')
            ->setAttrib('class', 'form-control')
            ->setRequired(true)
            ->addMultiOptions($options)
                ;
        
        $period = (new \Zend_Form_Element_Select('period'))
            ->setLabel('Período')
            ->setAttrib('class', 'form-control')
            ->setRequired(true);
        
//        $discipline = (new \Zend_Form_Element_Select('discipline'))
//            ->setLabel('Disciplina')
//            ->setAttrib('class', 'form-control')
//            ->setRequired(true);
        

        $this->addElement('button', 'Pesquisar', array('class' => 'btn btn-small btn-primary', 'type' => 'submit'));
        $this->getElement('Pesquisar')->removeDecorator('DtDdWrapper')->removeDecorator('Label');
        $this->getElement('Pesquisar')->addDecorator(array('wrapper' => 'HtmlTag'),   
                                                  array('tag' => 'div', 'class' => 'submit'))->setOrder(1000);
        
        $this->addElements([$course, $period]);
    }
    
}