<?php

class Default_Form_TutorTeam extends La_Form
{
    public function init() 
    {
        $this->setMethod('POST');
        $this->setAction('default/managerial/tutor-team');
        
        $courseDb = new Default_Model_DbTable_CourseCategories();
        $options = $courseDb->fetchOptions();
        
        $course = (new \Zend_Form_Element_Select('course'))
            ->setLabel('Curso')
            ->setAttrib('class', 'form-control')
            ->setRequired(true)
            ->addMultiOptions($options);
        
        $period = (new \Zend_Form_Element_Select('period'))
            ->setLabel('Período')
            ->setAttrib('class', 'form-control')
            ->setRequired(true);
        
        $visible = (new Zend_Form_Element_Checkbox('visible'))
            ->setLabel('Visível')
            ->setValue(1);

        
        $this->addElement('button', 'Pesquisar', array('class' => 'btn btn-small btn-primary', 'type' => 'submit'));
        $this->getElement('Pesquisar')->removeDecorator('DtDdWrapper')->removeDecorator('Label');
        $this->getElement('Pesquisar')->addDecorator(array('wrapper' => 'HtmlTag'),   
                                                  array('tag' => 'div', 'class' => 'submit'))->setOrder(1000);
        
        
        $this->addElements([$course, $period, $visible]);
    }
    
}