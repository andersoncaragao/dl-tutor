<?php

class Default_Model_DbTable_Course extends La_Db_Table
{
    protected $_name = "mdl_course";
    
    public function __construct() {
        
        $db = Zend_Registry::get('db-moodle');

        return parent::__construct($db);
    }

    public function getCourses()
    {
        $select = $this->getAdapter()->select()->from(['course' => $this->_name], ['course_id' => 'course.id', '*'])
                ->join(['category' => 'mdl_course_categories'], 'category.id = course.category', ['*'])
                ->where("course.id = ?", 988);

        return $select;
    }
    
    public function tutorXTurma()
    {
        $columns = ['c.id AS id', 
                'discipline'     => 'c.shortname',
                'students'       => "sum(CASE  WHEN u.department = 'estudante' THEN 1 ELSE 0 END)",
                'tutors_ead'     => "sum(CASE WHEN u.department = 'tutor_a_distancia' THEN 1 ELSE 0 END)",
                'tutorsXstudent' => "CASE WHEN sum(CASE WHEN u.department = 'tutor_a_distancia' THEN 1 ELSE 0 END) = 0 THEN 0 ELSE"
            . "(sum(CASE WHEN u.department = 'estudante' THEN 1 ELSE 0 END) / "
            . "sum(CASE WHEN u.department = 'tutor_a_distancia' THEN 1 ELSE 0 END)) END"
            ];
        
        $select = $this->getAdapter()->select()->from(['ra' => 'mdl_role_assignments'], '')
                ->join(['u' => 'mdl_user'], "ra.userid = u.id", '')
                ->join(['cxt' => 'mdl_context'], "ra.contextid = cxt.id", '')
                ->join(['c' => 'mdl_course'], "cxt.instanceid = c.id", '')
                ->group(['c.id'])
                ->columns($columns)
        ;
        
        return $select;
    }
    
    public function tutorXPolo($ids)
    {
        $select = $this->getAdapter()->select()->from(['ra' => 'mdl_role_assignments'], '')
                ->join(['u' => 'mdl_user'], "ra.userid = u.id", ['students' => 'count(u.id)', 'u.address', 'tag' => "substring(lastname from 4 for STRPOS(lastname, ']'))"])
                ->join(['cxt' => 'mdl_context'], "ra.contextid = cxt.id", '')
                ->join(['c' => 'mdl_course'], "cxt.instanceid = c.id", ['c.id'])
                ->group(['u.address', 'c.id', "substring(lastname from 4 for STRPOS(lastname, ']'))"])
                ->order('u.address')
                ->where("u.address <> ''")
                ->where("department = 'estudante'")
                ->where('c.id IN (?)', $ids)
                ;
        
        $selectGroup = $this->getAdapter()->select()->distinct()->from(['st' => $select], 
                ['students' => 'max(students)', 'address', 'polo' => "substring(st.tag from 4 for 3)","students" => "sum(students)"])
                ->joinLeft(['tp' => $this->getPresentTutor()], 'tp.tag = st.tag and tp.tag = st.tag', ['tutors' => 'CASE WHEN MAX(tutors) IS NULL THEN SUM(0) ELSE max(tutors) END'])
                ->group(['st.address', 'st.tag', 'tutors'])->order('polo')
                ;
        
        $selectReturn = $this->getAdapter()->select()
                ->from(['st' => $selectGroup], ["students" => 'MAX(students)', "tutors" => 'max(tutors)', "address", "polo", 'tutorXstudent' => 'CASE WHEN MAX(tutors) = 0 THEN SUM(0) ELSE max(students)/MAX(tutors) END'])
                ->order('address')
                ->group(['address', 'polo']);

        return $selectReturn;
    }
    
    public function getPresentTutor()
    {
        $select = $this->getAdapter()->select()->from(['u' => 'mdl_user'], ['tutors' => 'count(u.id)', 'u.address', 'tag' => "substring(lastname from 4 for STRPOS(lastname, ']'))"])
                ->order('u.address')
                ->where("u.address <> ''")
                ->where("u.lastname <> ''")
                ->where("department = 'tutor_presencial'")
                ->group(['u.address', 'tag' => "substring(lastname from 4 for STRPOS(lastname, ']'))"])
        ;
        return $select;
    }
    
    public function getEadTutor()
    {
        $select = $this->getAdapter()->select()->from(['u' => 'mdl_user'], ['tutors' => 'count(u.id)', 'u.address', 'tag' => "substring(lastname from 4 for STRPOS(lastname, ']'))"])
                ->order('u.address')
                ->where("u.address <> ''")
                ->where("u.lastname <> ''")
                ->where("department = 'tutor_a_distancia'")
                ->group(['u.address', 'tag' => "substring(lastname from 4 for STRPOS(lastname, ']'))"])
        ;
        return $select;
    }
   
}
