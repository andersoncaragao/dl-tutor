<?php

class Default_Model_DbTable_CourseCategories extends La_Db_Table
{
    protected $_name = "mdl_course_categories";
    protected $_categories = ['2'];

    public function __construct() {
        
        $db = Zend_Registry::get('db-moodle');

        return parent::__construct($db);
    }

    public function getCategories()
    {
        $columns = ['category_id' => 'category.id', 'cagegory_name' => 'name'];
        
        $select  = $this->getAdapter()->select()->from(['category' => $this->_name], '')
                ->where("category.id IN (?)", $this->_categories)
                ->columns($columns)
                ;

        return $select;
    }
    
    public function getCourses()
    {
        $columns = ['course_id' => 'course.id', 'course_name' => 'name'];
        $select  = $this->getAdapter()->select()->from(['course' => $this->_name], '')
                ->where("course.parent IN(?)", $this->_categories)
                ->columns($columns);

        return $select;
    }
    
    public function getPeriods()
    {
        $select  = $this->getAdapter()->select()->from(['course' => $this->getCourses()], ['*'])
                ->join(['period' => $this->_name], 'course.course_id = period.parent', 
                    ['period_id' => 'period.id', 'period_name' => 'name'])
                ->order('period.name')
                ;
        
        return $select;
        
    }
    
    public function getDisciplines()
    {
        $select = $this->getAdapter()->select()->from(['periods' => $this->getPeriods()], ['*'])
                ->join(['discipline' => 'mdl_course'], 'discipline.category = periods.period_id', 
                    ['discipline_id' => 'discipline.id', 'discipline_name' => 'fullname', 
                        'discipline_short' => 'shortname', 'semester' => "substring(fullname from '......$')"]);

        return $select;
        
    }
    
    public function getSemesters()
    {
        $select = $this->getAdapter()->select()->from(['periods' => $this->getPeriods()], [''])
                ->join(['discipline' => 'mdl_course'], 'discipline.category = periods.period_id', 
                    ['discipline_id' => "substring(fullname from '......$')", 'semester' => "substring(fullname from '......$')"])
                ->order("substring(fullname from '......$')");

        return $select;
        
    }
    
    public function getContext()
    {
        $select = $this->getAdapter()->select()->from(['context' => 'mdl_context'], 
            [
                'category' =>"split_part(path, '/', 2)",
                'course' =>"split_part(path, '/', 3)",
                'period' =>"split_part(path, '/', 4)",
                'discipline' =>"split_part(path, '/', 5)"
                ]);
        return $select;
        
    }
    
    public function fetchOptions($where = null, $type = 'course') {
        
        $data = ['[selecione]'];

        switch ($type) {
            case 'course':
                $select = $this->getCourses();
                $key    = 'course_id';
                $value  = 'course_name';
                break;
            
            case 'period':
                $select = $this->getPeriods();
                $key    = 'period_id';
                $value  = 'period_name';
                break;
            
            case 'discipline':
                $select = $this->getDisciplines();
                $key    = 'discipline_id';
                $value  = 'discipline_name';
                break;
            
            case 'semester':
                $select = $this->getSemesters()->group('discipline_id');
                $key    = 'discipline_id';
                $value  = 'semester';
                break;
            
            default:
                break;
        }
        
        if ($where) {
            $select = $this->setWhereOptions($where, $select);
        }
        $rowset = $this->getAdapter()->fetchAll($select);
        
        foreach ($rowset as $row) {
            if ($row[$key] && $row[$value]) {
                $data[$row[$key]] = $row[$value];
            }
        }

        return $data;
    }
    
    protected function setWhereOptions($values, $select)
    {
        if ($values) {
            foreach ($values as $key => $value) {
                if (is_numeric($value)) {
                    $select->where($key .'= ?', $value);
                }
            }
        }
        
        return $select;
    }
}