<?php

class Default_Model_DbTable_Tutor extends La_Db_Table
{
    protected $_name = "mdl_user";
    
    public function __construct() {
        
        $db = Zend_Registry::get('db-moodle');
        return parent::__construct($db);
    }

    public function tutorsPerTeam()
    {
        $select = $this->getAdapter()->select()->from($this->_name)
                ->where("department IN ('tutor_a_distancia', 'tutor_presencial')");

        return $this->getAdapter()->fetchAll($select);
    }
    
    public function tutorHit($dicipline, $timeInitial, $timeFinal)
    {
        $select = $this->getAdapter()->select()->from(['u'=> $this->_name ], ['firstname', 'id'])
                ->join(['l' => 'mdl_log'], 'l.userid = u.id ')
                ->where("department IN ('tutor_a_distancia')")
                ->where('course = ?', $dicipline)
                ->where("module = 'course'")
                ->where("time >= ?", $timeInitial)
                ->where("time <= ?", $timeFinal)
                ->order('l.id')
                ->order('u.id');

        $data = $this->getAdapter()->fetchAll($select);

        $timeI   = 0;
        $timeF   = 0;
        $i       = 0;
        $return  = [];
        
        foreach ($data as $d) {
            
            $date = (new Zend_Date($d['time'], \Zend_Date::TIMESTAMP))->toString('dd/MM/yyyy');
            
            if (!isset($return[$date][$d['userid']])) {
                $return[$date][$d['userid']]['sum_time'] = 0;
                $return[$date][$d['userid']]['name']     = $d['firstname'];
                $return[$date][$d['userid']]['time']     = $d['time'];
                $return[$date][$d['userid']]['day']      = $date;
            }
            
            if ($i == 0) {
                $timeI = $d['time'];
            }
            
            if ($i == 1) {
                $i         = 0; 
                $timeF     = $d['time'];
                $return[$date][$d['userid']]['sum_time'] += $timeF - $timeI;
            }
            $i++;
        }
        return $this->organizeTutorHit($return);
    }
    
    protected function organizeTutorHit($data)
    {
        if (!$data) {
            return [];
        }
        
        $return        = [];
        $dataR         = [];
        $labels['day'] = 'day';
        
        foreach ($data as $d) {
            foreach ($d as $value) {
                $name = substr($value['name'], 0, 15);
                
                if (!in_array($name, $labels)) {
                    $labels[$name] = $name;
                    $labelsGraph[] = $name;
                }
                $dataR[$value['day']]['day'] = $value['day'];
                $dataR[$value['day']][$name] = gmdate('H:i:s', $value['sum_time']);
            }
        }
        
        $dataG = [];
        
        foreach ($dataR as $dr) {
            foreach ($labels as $l) {
                if (!array_key_exists($l, $dr)) {
                    $dr[$l] = '0';
                }
            }
            $dataG[] = $dr;
        }
        
        $return['data']        = $dataG;
        $return['labels']      = $labels;
        $return['labelsGraph'] = $labelsGraph;
        
        return $return;
    }
}