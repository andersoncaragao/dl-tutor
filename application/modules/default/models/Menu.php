<?php

class Default_Model_Menu
{
    protected $_empresaId;
    
    protected $_headerId;
    protected $_link = 'scaffold/index/table/%s';
    
    protected $_tables = false;


    public function getHeaderId() {
        return $this->_headerId;
    }

    public function setHeaderId($headerId) {
        $this->_headerId = $headerId;
    }

    public function __construct() 
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        
        if (isset($identity->empresa_id) && $identity->empresa_id) {
            $this->_empresaId = $identity->empresa_id;
        }
        
        if (isset($identity->header_id) && $identity->header_id) {
            $this->_headerId = $identity->header_id;
        }
        
        return $this;
    }
    
    protected function getTables()
    {
        if ($this->_tables){
            return $this->_tables;
        }
        
        $db = Zend_Db_Table::getDefaultAdapter();
        return $db->listTables();
    }


    public function getLinks()
    {
        $links = []; 
        
        foreach ($this->getTables() as $table) {
            $links[$table] = $this->prepareLink($table);
        }
        
        return $links;
        
    }
    
    protected function prepareLink($table)
    {
        $return       = ['title' => ucwords(str_replace('_', ' ', $table))];
        $controller = str_replace('_', '-', $table);
        
        $return['link']   = sprintf($this->_link, $table);
        $return['class']  = 'fa fa-list-alt';
        $return['active'] = $controller;
        
        return $return;
    }

}
