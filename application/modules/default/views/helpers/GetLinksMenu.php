<?php

class Zend_View_Helper_GetLinksMenu extends Zend_View_Helper_Abstract {

    public function getLinksMenu() 
    {
        $menu  = new Default_Model_Menu();
        $links = $menu->getLinks();

        return $links;
    }
}