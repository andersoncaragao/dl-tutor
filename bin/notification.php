<?php
$args = $_SERVER['argv'];

if ($_SERVER['argc'] != 2) {
    echo "Falha ao atualizar o banco\n";
    var_dump($_SERVER['argv']); exit;
}

define('APPLICATION_ENV', $args[1]);

defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/modules'),
    get_include_path()
)));
$logFile = APPLICATION_PATH . '/../data/log/notification.txt';
require APPLICATION_PATH . '/../vendor/autoload.php';

$autoloader = Zend_Loader_Autoloader::getInstance();

$log = Zend_Log::factory([['writerName'   => 'Stream',
                           'writerParams' => ['stream'   => $logFile]
                         ]]);

echo "\n----------------------\n";
$filename = APPLICATION_PATH . '/configs/application.ini';
echo "Iniciando: ". $filename . "\n";
$application = new La_Application(APPLICATION_ENV, $filename);
$application->bootstrap();

$newTable = new La_Db_Table('new');
$news = $newTable->fetchAll('push = 1 AND publication < now() AND draft = 0');
$response = null;

if (count($news)) {
    foreach ($news as $row) {
        $logMessage = sprintf("Enviando: %s(%d)\n", $row->title, $row->id);
        $log->debug($logMessage);
        echo $logMessage;
        $message = new Zend_Mobile_Push_Message_Gcm();
        $message->setData(array(
            "type" => "NewsItem",
            "contentId" => $row->id,
            "title" => $row->title,
            "subtitle" => substr(strip_tags($row->description),0,100)
        ));
        
        $registrationIds = Default_Model_New::getRegistrations($row->id);
        
        if (count($registrationIds)) {
            foreach ($registrationIds as $rid) {
                $logMessage = sprintf("Para: %s\n", $rid);
                echo $logMessage;
                $log->debug($logMessage);
                $message->addToken($rid);
            }
        
            $gcm = new Zend_Mobile_Push_Gcm();
            $gcm->setApiKey('AIzaSyBkECIav0rpbAjW6PtFrvb5MLAvlmDjpdo');

            $response = $gcm->send($message);
        
            $row->push = 2;
            $row->save();
            
            // handle all errors and registration_id's
            foreach ($response->getResults() as $k => $v) {
                if (isset($v['registration_id'])) {
                    $logMessage = sprintf("%s has a new registration id of: %s\r\n", $k, $v['registration_id']);
                    echo $logMessage;
                    $log->debug($logMessage);
                }
                if (isset($v['error'])) {
                    $logMessage = sprintf("%s had an error of: %s\r\n", $k, $v['error']);
                    echo $logMessage;
                    $log->debug($logMessage);
                }
                if (isset($v['message_id'])) {
                    $logMessage = sprintf("%s was successfully sent the message, message id is: %s\r\n", $k, $v['message_id']);
                    echo $logMessage;
                    $log->debug($logMessage);
                }
            }
        }
    }
}
echo "\n----------------------\n";