<?php
$args = $_SERVER['argv'];

if ($_SERVER['argc'] != 2) {
    echo "Falha ao atualizar o banco\n";
    var_dump($_SERVER['argv']); exit;
}

define('APPLICATION_ENV', $args[1]);

defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/modules'),
    get_include_path()
)));

require APPLICATION_PATH . '/../vendor/autoload.php';

$autoloader = Zend_Loader_Autoloader::getInstance();

function mergeOptions(array $array1, $array2 = null)
{
    if (is_array($array2)) {
        foreach ($array2 as $key => $val) {
            if (is_array($array2[$key])) {
                $array1[$key] = (array_key_exists($key, $array1) && is_array($array1[$key]))
                              ? mergeOptions($array1[$key], $array2[$key])
                              : $array2[$key];
            } else {
                $array1[$key] = $val;
            }
        }
    }
    return $array1;
}

$aclConfig = new Zend_Config_Ini(realpath(APPLICATION_PATH . '/modules/auth/configs/acl.ini'), 'acl');
$aclOptions = $aclConfig->toArray();

foreach ($aclOptions['config'] as $tmp) {
    $tmpConfig = new Zend_Config_Ini($tmp, 'acl');
    $aclOptions = mergeOptions($aclOptions, $tmpConfig->toArray());
}

echo "\n----------------------\n";
$filename = APPLICATION_PATH . '/configs/application.ini';
echo "Iniciando: ". $filename . "\n";
$application = new La_Application(APPLICATION_ENV, $filename);
$application->bootstrap();

$role         = new Auth_Model_DbTable_Role();
$resource     = new Auth_Model_DbTable_Resource();
$roleResource = new Auth_Model_DbTable_RoleResource();

echo "Atualizando perfis de usuários\n";
foreach ($aclOptions['roles'] as $roleName) {
    $row = $role->fetchRow(array('name = ?' => $roleName));

    if (!$row) {
        $data = array('name' => $roleName);
        $groupId = $role->insert($data);
    } 
    
    if ($roleName == 'Todos') {
        $todosId = $row ? $row['id'] : $groupId;
    }
    
    if ($roleName == 'Admin') {
        $adminId = $row ? $row['id'] : $groupId;
    }
}

echo "Atualizando resources\n";
foreach ($aclOptions['resources'] as $module => $privileges) {
    foreach ($privileges as $privilege) {
        $where = array('module = ?' => $module,
                       'privilege = ?' => $privilege);
        $row = $resource->fetchRow($where);
        
        if (!$row) {
            $id = $resource->createRow(array('module' => $module, 
                                             'privilege' => $privilege))->save();
            if ($id) {
                $roleResource->createRow(array('auth_resource_id' => $id, 
                                               'auth_role_id' => $adminId))->save();
            }
        }
    }
}

echo "Atualizando privilegios para todos\n";
foreach ($aclOptions['rules'] as $module => $privileges) {
    foreach ($privileges as $privilege) {
        $where = array('module = ?' => $module,
                       'privilege = ?' => $privilege);
        $row = $resource->fetchRow($where);
        if ($row) {
            $where = array('auth_resource_id = ?' => $row->id,
                           'auth_role_id = ?' => $todosId);
            $existedRow = $roleResource->fetchRow($where);
            if (!$existedRow) {
                $roleResource->createRow(array('auth_resource_id' => $row->id, 
                                               'auth_role_id' => $todosId))->save();
            }
        }
    }
}

echo "Atualizado com sucesso\n----------------------\n";

