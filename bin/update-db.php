<?php
$args = $_SERVER['argv'];

if ($_SERVER['argc'] != 3) {
    echo "Falha ao atualizar o banco\n";
    var_dump($_SERVER['argv']); exit;
}

define('ENV', $args[1]);
define('VERSION', $args[2]);
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

set_include_path(APPLICATION_PATH . '/../library');

require 'La/functions.php';
require 'Zend/Application.php';
require 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();

function updatedb($env, $configFile) {
    $sqlFile = APPLICATION_PATH . '/../data/sql/update/' . VERSION . '.sql';
    
    if (!file_exists($sqlFile)) {
        echo "Arquivo sql não existente (";
        echo $sqlFile . ")\n";
        exit;
    }
    
    $sql     = explode(';', str_replace("\n", ' ', file_get_contents($sqlFile)));
    $config  = new Zend_Config_Ini($configFile, $env);
    $options = $config->toArray();
    $error   = false;
    
    foreach ($options['config'] as $merge) {
        $mergeConfig = new Zend_Config_Ini($merge, $env);
        $options = mergeOptions($mergeConfig->toArray(), $options);
    }

    $db = Zend_Db::factory($options['resources']['db']['adapter'], $options['resources']['db']['params']);
    
    echo "Executando arquivo: " . $sqlFile . "\nConfiguracoes de banco:\n"; 
    print_r($options['resources']['db']['params']);
    foreach ($sql as $command) {
        try {
            if ($command) {
                $db->query($command);
            }
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            echo substr($command, 0, 60) . "..." . PHP_EOL . PHP_EOL;
            continue;
        }
    }
}

echo "\n---------------------------------------------------------------\n";
$configFile = APPLICATION_PATH . '/configs/application.ini';
echo "Iniciando: ". $configFile . "\n";
updatedb(ENV , $configFile); 

echo "\nAtualizado com sucesso!";
echo "\n---------------------------------------------------------------\n\n";
