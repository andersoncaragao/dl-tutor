<?php
define('APPLICATION_ROOT', realpath(dirname(__FILE__) . '/..'));
define('APPLICATION_PATH', APPLICATION_ROOT . '/application');
define('LIBRARY_PATH'    , APPLICATION_ROOT . '/vendor');
define('APPLICATION_ENV' , getenv('APPLICATION_ENV') ?: 'production');

require(LIBRARY_PATH . '/autoload.php');
require(LIBRARY_PATH . '/abdala/la/src/La/functions.php');

$config = APPLICATION_PATH . '/configs/application.ini';
$application = new La_Application(APPLICATION_ENV, $config);
$application->bootstrap()->run();