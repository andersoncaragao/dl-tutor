(function($){
    $.fn.selectPlus = function(url) {
        return this.each(function(i){
            var $plusImage = $('<a style="float: right"><img src="img/icon_add.png"></a>'),
                $popup = $('<div class="select-plus-popup"><span style="background: white;">Carregando...</span></div>'),
                $this = $(this),
                id = $this.attr('id'),
                table = this.name.split("_id")[0],
                dynamicUrl = null;
        
            if (!url) {
                dynamicUrl = 'scaffold/form/format/html/table/' + table;
            }
            
            $popup.attr('id', 'popup-' + id);
            $(document.body).append($popup);
    
            $plusImage.attr('href', url || dynamicUrl);
            $plusImage.find('img').css('margin', '4px 0 0 4px');
            $plusImage.click(function(){
                $.get(this.href, {selectId:id},function(content){
                    bootbox.dialog({message:content, closeButton: false});
                })
                return false;
            });
    
            $this.parent().prepend($plusImage);
        });
    };
})(jQuery);

function selectPlusSetAction(formId, selectId) {
    $('#' + formId).submit(function(){
        var url = this.action + '?format=json',
            messages = '';
        $.post(url, $(this).serialize(), function(response){
            if (response.id) {
                var $option = $('<option>'),
                    $select = $('#' + selectId),
                    value = response.id;
                    name = response.name ? response.name : "Novo registro";
                    
                $option.val(response.id);
                $option.text(name);
                
                if ($select.prop('multiple')) {
                    value = $select.val() + [value];
                }
                
                $select.append($option).val(response.id).trigger('chosen:updated');
                bootbox.hideAll();
            } else {
                messages = response.messages.join("\n") + "\n";
                for (var errors in response.errors) {
                    for (var error in response.errors[errors]) {
                        var label = $('label[for=' + errors + ']:last').text();
                        messages += "\n" + label + ": " + response.errors[errors][error];
                    }
                }
                alert(messages);
            }
        });
        return false;
    });
}