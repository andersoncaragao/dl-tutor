/** Mascaras */
function mascara(o,f) {
    setTimeout(function(){
        o.value = f(o.value);
    }, 10);
}

function soNumeros(v){
    return v.replace(/\D/g,"");
}

function mvalor(v){
    v = v.replace(/\D/g,"");
    v = v.replace(/(\d)(\d{8})$/,"$1.$2");
    v = v.replace(/(\d)(\d{5})$/,"$1.$2");
    v = v.replace(/(\d)(\d{2})$/,"$1,$2");
    return v;
}

//Para a navegação em tabs funcionar em conjunto com a utilização da tag base
$.fn.__tabs = $.fn.tabs;
$.fn.tabs = function (a, b, c, d, e, f) {
    var base = location.href.replace(/#.*$/, '');
    $('ul>li>a[href^="#"]', this).each(function () {
        var href = $(this).attr('href');
        $(this).attr('href', base + href);
    });
    $(this).__tabs(a, b, c, d, e, f);
};

$(document).ready(function(){
    $('.zend_form').find('input, textarea, select').focus(function(){
        $(this).parent().find('.errors').hide('slow');
    });
});