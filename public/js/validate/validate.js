Validator = function(form, data) {
    this.validators    = [];
    this.errorMessages = [];
    this.form          = document.getElementById(form) || document.forms[0];
    
    this.init(data);
    
    Validator.Event.on(this.form, "submit", this.validate, this);
}

Validator.prototype = {
    
    init: function(data) {
        var validators = eval("(" + data + ")"), validator, i = 0, fnName;
        for(id in validators) {
            validator = validators[id];
            
            for (i = 0; i < validator.length; i++) {
                fnName = validator[i];
                
                if (typeof validator[i] == "object") {
                    fnName = validator[i][0];
                }
                
                if (typeof Validate[fnName] == "function") {
                    this.add(id, Validate[fnName], fnName, validator[i][2]);
                }
            }
        }
    },
    
    add: function(id, fn, fnName, param) {
        if (!(id && fn)) {
            console.log('Wrong object arguments. Missing: id ' + id + 'or func' + fn + '\n\n');
            return false;
        }

        if (typeof param != 'object' && typeof param != 'undefined') {
            param = Array.prototype.slice.call(arguments, 3);
        }

        var values = {id: id, fn: fn, fnName: fnName, param: param || null};
        
        this.validators.push(values);
    },

    validate: function(ev) {
        this.clearMessages();
        
        var i = 0, valid = true, args = [], el;
        
        for (; i < this.validators.length; i++) {
            if (this.inForm(this.validators[i].id)) {
                args = [];
                el  = this.form.elements[this.validators[i].id];
                
                args[0] = el.value;
                args    = args.concat(this.validators[i].param);
                args    = this._valueByIdOrName(args);
                
                if(!this.validators[i].fn.apply({element: el, validator: this}, args)) {
                    valid = false;
                    this.addError(i);
                }
            }
        }
 
        if (!valid) {
            Validator.Event.stopEvent(ev);
        }
    },

    inForm: function(id) {
        var n = this.form.elements.length, i = 0;
	  	
        for (; i < n ; i++) {
            if (this.form.elements[i].id == id) {
                return true;
            }
        }
        return false;
    },

    _valueByIdOrName: function(param) {
        var i = 1, el, els;
        
        for(; i < param.length; i++ ){
            el = document.getElementById(param[i]);
            
            if (el) {
                param[i] = el.value;
            } else {
                els = document.getElementsByName(param[i]);
	
                if (els.length) {
                    param[i] = els[0].value;
                }
            }
        }
        return param;
    },
  	
    addError: function(index) {
        var ul = document.createElement('ul'),
            li = document.createElement('li'),
            el = document.getElementById(this.validators[index].id);

        ul.className = 'errors';
        li.innerHTML = Validate.messages[this.validators[index].fnName] || "Campo inválido";
        ul.appendChild(li);
  		
        el.parentNode.insertBefore(ul, el.nextSibling);
    },
	
    clearMessages: function(childs) {
        var i = 0, childsChilds;
        
        childs = childs || this.form.childNodes;

        for(; i < childs.length; i++) {
            childsChilds = childs[i].childNodes;

            if (childsChilds.length){
                this.clearMessages(childsChilds);
            }

            if (childs[i].className == 'errors') {
                childs[i].parentNode.removeChild(childs[i]);
            }
        }
    },
    
    clear: function() {
        this.validators = [];
        this.errorMessages = [];
    }
}

Validate = {};
Validate.messages = {
    NotEmpty: "Campo obrigatório",
    StringLength: "Tamanho inválido"
};

Validate.NotEmpty = function(value) {
    if (!value.replace( /^\s*|\s*$/g, "")) {
        return false;
    }
    return true;
}

Validator.Event = {};
Validator.Event.listener = {};

Validator.Event.on = function(element, name, fn, scope) {
    if (scope) {
        Validator.Event.listener[element.id] = fn;
        fn = function(e){
            return Validator.Event.listener[element.id].call(scope, e || window.event);
        }
    }

    if (element.addEventListener) {
        element.addEventListener(name, fn);
    } else if (element.attachEvent) {
        element.attachEvent('on' + name, fn);
    }
}

Validator.Event.stopEvent = function(event) {
    if (event.stopPropagation) {
        event.preventDefault();
        event.stopPropagation();
    }
    
    event.returnValue = false;
    event.cancelBubble = true; 
}

Validator.Event.key = function(ev) {
    return ev.which || ev.keyCode;
}

/*------------------- Number ---------------------------*/
Validate.GreaterThan = function(value, param) {
	return (value >= param);
}

Validate.LessThan = function(value, param) {
    return (value <= param);
}

Validate.Between = function(value, start, end) {
    return (value >= start && value <= end);
}

Validate.Digits = function(value) {
    if(!value) { 
        return true;
    }
    
    return /^\d+$/.test(value);
}

Validate.Int = function(value) {
    if (!value) {
        return true;
    }
    
    return !isNaN(value);
}
/*------------------- String ---------------------------*/
Validate.identical = function(value, param) {
	return (value == param);
}

Validate.Hostname = function(value) {
    if (!value) {
    	return true;
    }
    // contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
}

Validate.EmailAddress = function(value) {
    if (!value) {
    	return true;
    }
    // contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_Validate/
    return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
}

Validate.Minlength = function(value, length) {
    return value.length >= length;
}

Validate.Maxlength = function(value, length) {
    return value.length <= length;
}

Validate.StringLength = function(value, min, max) {
    if (this.element.tagName.toLowerCase() == "select") {
        return true;
    }
    
    if (this.element.maxLength != value) {
        max = this.element.maxLength;
    }
    
    return (value.length >= min && value.length <= max);
}

/*------------------- Date ---------------------------*/
Validate.Date = function(value) {
    if (value) { 
        var regex = value.match(/^(0?[1-9]|[1-2][0-9]|3[0-1])\/(0?[1-9]|1[0-2])\/((?:19|[2][0-1])?\d{2})$/);
  	
        if (regex == null) {
            return false;
        }
	
        if (regex[2] == 2 && regex[1] > 29) {
            return false;
        }

        if (regex[2] == 2 && regex[1] == 29 && (regex[3] % 4)) {
            return false;
        }
	
        if ((regex[2] == 4 || regex[2] == 6 || regex[2] == 9 || regex[2] == 11) 
            && (regex[1] == 31))
        {
            return false;
        }
	}
    
    return true;
}

Validate.DateGreaterThan = function(value, date) {
    return Validate._dateGreaterLessThan(Validate.greaterThan, value, date);
}

Validate.DateLessThan = function(value, date) {
    return Validate._dateGreaterLessThan(Validate.lessThan, value, date);
}

Validate._dateGreaterLessThan = function(fn, value, date) {
    if (date && value) {
        value = Validate._dateTransform(value);
        date  = Validate._dateTransform(date);

        if (!(value && date)) {
            return false;
        }

        return fn(value, date);
    }
    
    return true;
}

Validate._dateTransform = function(value) {
    value = value.split("/");
    return parseInt(value[2] + value[1] + value[0]);
}

Validate.DateBetween = function(value, dateStart, dateEnd) {
    if( value == "" ){
        return true;
    }

    value     = Validate._dateTransform(value);
    dateStart = Validate._dateTransform(dateStart);
    dateEnd   = Validate._dateTransform(dateEnd);

    if(!(value && dateStart && dateEnd)) {
        return false;
    }

    return Validate.between(value, dateStart, dateEnd);
}
/*------------------- Brazil ---------------------------*/
Validate.Cpf = function(value) {
    var sum = 0,remain,y;

    if (value) { 
        value = value.replace(".","").replace(".","").replace("-","");

        if (value.length != 11 || value == "00000000000" || value == "11111111111" || value == "22222222222" ||value == "33333333333" || value == "44444444444" || value == "55555555555" || value == "66666666666" || value == "77777777777" || value == "88888888888" || value == "99999999999") {
            return false;
        }

        for (y=0; y < 9; y ++) {
            sum += parseInt(value.charAt(y)) * (10 - y);
        }

        remain = 11 - (sum % 11);

        if (remain == 10 || remain == 11) {
            remain = 0;
        }

        if (remain != parseInt(value.charAt(9))) {
            return false;
        }

        sum = 0;

        for (y = 0; y < 10; y ++) {
            sum += parseInt(value.charAt(y)) * (11 - y);
        }

        remain = 11 - (sum % 11);

        if (remain == 10 || remain == 11) {
            remain = 0;
        }

        if (remain != parseInt(value.charAt(10))) {
            return false;
        }
    }

    return true;
}

Validate.Cnpj = function(value) {
    var n = [],sum,result;

    if (!value) {
        return true
    }
    
    value = value.replace(".","").replace(".","").replace("/","").replace("-","");

    if (value == "00000000000000") {
        return false;
    }

    n.push(0);

    for (var i = 0; i < value.length; i++) {
        n.push( parseInt( value.substr(i , 1 )));
    }

    sum = (n[1] * 5) + (n[2] * 4) + (n[3] * 3) + (n[4] * 2) + (n[5] * 9) + (n[6] * 8) + (n[7] * 7) + (n[8] * 6) + (n[9] * 5) + (n[10] * 4) + (n[11] * 3) + (n[12] * 2);
    sum = sum - (11 * (parseInt(sum / 11)));

    result = 11 - sum;
    if (sum == 0 || sum == 1) {
        result = 0;
    } 

    if (result == n[13]) {
        sum = n[1] * 6 + n[2] * 5 + n[3] * 4 + n[4] * 3 + n[5] * 2 + n[6] * 9 + n[7] * 8 + n[8] * 7 + n[9] * 6 + n[10] * 5 + n[11] * 4 + n[12] * 3 + n[13] * 2;
        sum = sum - (11 * (parseInt(sum/11)));

        if (sum == 0 || sum == 1) {
            result = 0;
        } else {
            result = 11 - sum;
        }

        if (result == n[14]) {
            return true;
        }
    }
    
    return false;
}

Validate.Cep = function(value) {
    if (value) {
        return /^\d{5}\-\d{3}$/.test(value);
    }
    
    return true;
}
