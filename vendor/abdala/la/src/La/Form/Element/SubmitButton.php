<?php

class La_Form_Element_SubmitButton extends Zend_Form_Element_Submit
{
    public function __construct($spec, $options = null)
    {
        parent::__construct($spec, $options);
        $this->removeDecorator('DtDdWrapper');
        $this->setDecorators(['ViewHelper',
            'Errors',
            ['HtmlTag',
                ['tag' => 'div', 'class' => 'btn btn-small btn-primary']
            ]
        ]);
    }
} 