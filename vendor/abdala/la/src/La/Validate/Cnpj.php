<?php
class La_Validate_CpfCnpj extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
        $value   = (string) $value;
        $cnpj    = new Zebra_Validate_Cnpj();
        $isValid = $cnpj->isValid($value);
        
        $this->_errors   = $cnpj->getErrors();
        $this->_messages = $cnpj->getMessages();
        return $cnpj->isValid($value);
        
    }
}