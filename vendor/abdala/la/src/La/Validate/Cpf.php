<?php
class La_Validate_Cpf extends Zend_Validate_Abstract
{
    public function isValid($value)
    {
        $value   = (string) $value;
        $cpf     = new Zebra_Validate_Cpf();
        $isValid = $cpf->isValid($value);
        
        $this->_errors   = $cpf->getErrors();
        $this->_messages = $cpf->getMessages();
        
        return $isValid;
    }
}