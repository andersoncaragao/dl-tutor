<?php
/**
 * @category Zebra
 * @package  Zebra_Validate
 */

/**
 * @see Zend_Validate_Abstract
 */
require_once 'Zend/Validate/PostCode.php';

/**
 * @uses     Zend_Validate_PostCode
 * @category Zebra
 * @package  Zebra_Validate
 */
class Zebra_Validate_Cep extends Zend_Validate_PostCode
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->_locale = 'pt_BR';
        parent::__construct();
    }
}
